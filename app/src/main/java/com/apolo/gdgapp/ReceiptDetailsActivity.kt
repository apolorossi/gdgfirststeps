package com.apolo.gdgapp

import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_receipt_details.*

class ReceiptDetailsActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receipt_details)

        val receipt = intent.extras?.get("receipt") as ReceiptModel

        Handler().postDelayed({
            receipt_title.text = receipt.receiptTitle
            receipt_descrition.text = receipt.receiptDescription
            loader.visibility = View.GONE
        }, 3000)


    }
}