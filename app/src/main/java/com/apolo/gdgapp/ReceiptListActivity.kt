package com.apolo.gdgapp

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.apolo.gdgapp.mockData.APIClass
import kotlinx.android.synthetic.main.activity_receipt_list.*

class ReceiptListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receipt_list)

        val receiptListAdapter = ReceiptListAdapter(
            LayoutInflater.from(this), APIClass.getReceiptMockedList()
        ) {
            Toast.makeText(this, "Receita selecionada: ${it.receiptTitle}", Toast.LENGTH_LONG).show()

            val intent = Intent(this, ReceiptDetailsActivity::class.java)

            intent.putExtras(
                Bundle().apply {
                    putSerializable("receipt", it)
                }
            )

            startActivity(intent)
        }

        receipt_list.adapter = receiptListAdapter
        receipt_list.addItemDecoration(
            DividerItemDecoration(
                this,
                (receipt_list.layoutManager as LinearLayoutManager).orientation
            )
        )

    }

}