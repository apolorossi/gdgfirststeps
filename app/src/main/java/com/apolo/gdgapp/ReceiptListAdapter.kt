package com.apolo.gdgapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ReceiptListAdapter(private val layoutInflater: LayoutInflater,
                         private val receiptList : List<ReceiptModel>,
                         private val onItemClick : (ReceiptModel) -> Unit) : RecyclerView.Adapter<ReceiptListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReceiptListViewHolder {
        return ReceiptListViewHolder(layoutInflater.inflate(R.layout.receipt_item_view_holder, parent, false))
    }

    override fun getItemCount() = receiptList.size

    override fun onBindViewHolder(holder: ReceiptListViewHolder, position: Int) {
        holder.bind(receiptList[position], onItemClick)

    }
}