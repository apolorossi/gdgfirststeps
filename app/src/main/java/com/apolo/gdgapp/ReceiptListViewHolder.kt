package com.apolo.gdgapp

import android.view.View
import android.widget.TextView
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.receipt_item_view_holder.view.*

class ReceiptListViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

    fun bind(receipt : ReceiptModel, onItemClick : (ReceiptModel) -> Unit) {
        itemView.apply {
            receipt_name.text = receipt.receiptTitle
            receipt_duration.text = receipt.receiptDuration.toString()
            difficult_level.text = receipt.receiptLevel
            setOnClickListener { onItemClick(receipt) }
        }

    }

}