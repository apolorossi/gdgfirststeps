package com.apolo.gdgapp

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

data class ReceiptModel(
    val receiptTitle: String?,
    val receiptDuration: Int,
    val receiptLevel: String,
    val receiptDescription: String
): Serializable