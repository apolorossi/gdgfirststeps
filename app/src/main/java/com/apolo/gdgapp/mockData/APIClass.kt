package com.apolo.gdgapp.mockData

import com.apolo.gdgapp.ReceiptModel

class APIClass {
    companion object {
        fun getReceiptMockedList() = listOf(
            ReceiptModel(
                "Arroz branco",
                20,
                "Iniciante",
                "Faça uma refoga do arroz com azeite, óleo ou manteiga, dependendo do tipo do arroz. Uma boa refoga do arroz, evitando mexer com muita vigorosidade para não quebrar os grãos. Na hora da cocção com o líquido, evitar ficar mexendo. Fazer uma boa refoga, adicionar o líquido na medida certa e aguardar o tempo correto de cocção, sem mexer. O arroz vai ficar soltinho"
            ), ReceiptModel(
                "Almôndegas de grão de bico",
                25,
                "Iniciante",
                "Deixe os grãos de bico de molho por cerca de 24h. Coloque para cozinhar com uma pitada de sal. Em seguida, bata o grão de bico no liquidificador até virar uma farofinha. Em uma tigela, misture o grão de bico com hortelã, salsinha, cebolinha, manjericão, alho e sal. Misture bem com as mãos. Adicione a chia e a farinha de arroz até dar liga. Faça bolinhas com a massa de grão de bico e coloque em uma assadeira. Leve ao forno em uma temperatura a 180º até dourar. Se desejar, adicione molho de tomate por cima."
            ), ReceiptModel(
                "Abobrinha à parmegiana recheada",
                50,
                "Intermediario",
                "Corte a abobrinha em tiras com a ajuda de um cortador de legumes ou com uma faca bem afiada. Deixe cozinhar por cerca de 1 minuto. Em uma vasilha, misture o frango desfiado e o creme de ricota light. Depois, adicione a muçarela e incorpore na mistura. Tempere com sal e pimenta a gosto. Em um prato fundo, jogue o molho de tomate. Pegue uma tira de abobrinha, coloque um pouco do recheio e faça um rolinho. Coloque no prato os rolinhos com as pontas da abobrinha para baixo e adicione molho de tomate e muçarela por cima"
            ), ReceiptModel(
                "Arroz com frutas cristalizadas sem uvas passas",
                32,
                "Iniciante",
                "Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, "
            ), ReceiptModel(
                "Arroz com lula",
                40,
                "Avançado",
                "Primeiramente, prepare o feijão fradinho e o arroz branco normalmente. Frite o bacon e a calabresa. Frite costelinha suína em óleo quente no tacho ou frigideira grande por aproximadamente 20 minutos, ou até cozinhar e ficar crocante. Em outra panela grande, frite a carne seca na manteiga por três minutos. Abaixe o fogo e acrescente o feijão e o arroz, mexendo bem sempre. Acrescente o queijo coalho, a cebola, o bacon e a calabresa. Misture tudo por 5 minutos."
            ), ReceiptModel(
                "Arroz de marisco",
                30,
                "Intermediario",
                "Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, "
            ), ReceiptModel(
                "Arroz doce gourmet",
                20,
                "Iniciante",
                "Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, "
            ), ReceiptModel(
                "Arrumadinho",
                27,
                "Iniciante",
                "Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, "
            ), ReceiptModel(
                "Atoladinho de carne seca",
                60,
                "Intermediario",
                "Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, "
            ), ReceiptModel(
                "Bacalhau à lagareiro",
                45,
                "Avançado",
                "Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, "
            ), ReceiptModel(
                "Bacalhau à Zé do Pipo",
                40,
                "Intermediario",
                "Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, "
            ), ReceiptModel(
                "Bacalhau crocante",
                50,
                "Iniciante",
                "Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, "
            ), ReceiptModel(
                "Bacalhau gratinado",
                55,
                "Intermediario",
                "Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, "
            ), ReceiptModel(
                "Batata suíça de frango com catupiry",
                33,
                "Avançado",
                "Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, "
            ), ReceiptModel(
                "Berinjela à parmegiana",
                48,
                "Intermediario",
                "Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, Pegue o macarrão e ferva na agua até ficar aldente, "
            )
        )
    }
}